#ifndef HEAP_H
#define HEAP_H

#include<iostream>
//MinHeap
template <typename T>
class Heap
{
private:
    T* values;
    int dimVect;
    int capVect;
public:
    Heap(int capVect){
        values = new T[capVect];
        this->capVect = capVect;
        this->dimVect = 0;
    }

    ~Heap(){
        delete[] values;
    }

    void printValues(){
        for(int i=0; i < dimVect; i++)
            std::cout << values[i] << " ";
    }
    /*Nu am inteles ce ar trebui sa faca functiile parent, leftSubtree si rightSubtree. Cerinta nu e clara.
      Am presupus ca pur si simplu intorc elemente. */
    T parent(int poz){
        return values[poz];
    }

    T leftSubtree(int poz){
        return values[poz*2+1];
    }

    T rightSubtree(int poz){
        return values[poz*2+2];
    }


    void pushUp(int poz){
        if(!poz)
            return;

        if(values[poz] < values[(poz-1)/2])
        {
            T aux = values[poz];
            values[poz] = values[(poz-1)/2];
            values[(poz-1)/2] = aux;
            pushDown((poz-1)/2);
        }
    }

    void pushDown(int poz){
        if(poz >= dimVect)
            return;

        //left
        if(values[poz] > values[2*poz+1])
        {
            T aux = values[poz];
            values[poz] = values[2*poz+1];
            values[2*poz+1] = aux;
            pushDown(2*poz+1);
        }

        //right
        if(values[poz] > values[2*poz+2])
        {
            T aux = values[poz];
            values[poz] = values[2*poz+2];
            values[2*poz+2] = aux;
            pushDown(2*poz+2);
        }


    }

    void insert(T x){
        if(dimVect==capVect)
        {
            std::cout << "Heap is full";
            return;
        }

        values[dimVect] = x;
        dimVect++;
        pushUp(dimVect - 1);

        printValues();
        std::cout<<std::endl;
    }

    T peek(){
        return values[0];
    }

    T extractMin(){
        T aux = values[0];
        values[0] = values[dimVect-1];
        values[dimVect-1] = aux;

        dimVect--;

        pushDown(0);

        return aux;
    }
};

#endif // HEAP_H
